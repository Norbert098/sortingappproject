package com.epam.project;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(JUnitParamsRunner.class)
public class UserTest {

    User user = new User();

    //@Parameterized.Parameters
    public static Object[] positiveTestValuesOfArrayLength() {
        return new Object[] {
                new Object[]{9, 9},
                new Object[]{1, 1},
        };
    }

    public static Object[] negativeTestValuesOfArrayLength() {
        return new Object[] {
                new Object[]{-1, 0},
                new Object[]{-2, 1},
                new Object[]{-3, 2},
                new Object[]{-4, 3},
                new Object[]{-5, 4},
                new Object[]{-6, 5},
                new Object[]{-7, 6},
                new Object[]{-8, 7},
                new Object[]{-9, 8},
                new Object[]{-10, 9},
        };
    }

    @Test
    @Parameters(method = "positiveTestValuesOfArrayLength")
    public void cornerCaseTestingOfLengthArrayPositive(int value1, int expectedValue) {
        assertEquals(user.getLengthOfArray(expectedValue), user.getLengthOfArray(value1));
        assertThat(user.getLengthOfArray(expectedValue), lessThan(10));
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "negativeTestValuesOfArrayLength")
    public void cornerCaseTestingOfLengthArrayNegative(int value1, int expectedValue) {
        assertNotEquals(user.getLengthOfArray(expectedValue), user.getLengthOfArray(value1));
        assertThat(user.getLengthOfArray(expectedValue), greaterThan(0));

    }
}
