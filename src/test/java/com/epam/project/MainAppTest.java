package com.epam.project;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainAppTest {

    Calc calc = new Calc();
    User user = new User();

    @Test(expected = NullPointerException.class)
    public void valuesShouldNotBeNull() {
        //given
        int[] given = null;
        //int[] expected = {1, 2, 3, 7, 8, 10};
        //when
        calc.sortIntegerInAscendingOrder(given);
        //then
        assertNotNull("The object you enter return null" , given);
    }

    @Test
    public void shouldBeInAscendingOrder() {
        //given
        int[] given = {8, 10, 2, 7, 3, 1};
        int[] expected = {1, 2, 3, 7, 8, 10};
        //when
        calc.sortIntegerInAscendingOrder(given);
        //then
        assertArrayEquals(expected, given);
    }
}