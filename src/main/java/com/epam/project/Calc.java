package com.epam.project;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Class with logic:
 *  - adding to the Array userInput
 *  - print results
 *  - sorting in ascending order
 *
 * @author Norbert Borek
 */
public class Calc {
    /**
     * Method which add Integer to array
     * @param reader
     * @throws IOException
     */

     int[] getIntsAndAdd(BufferedReader reader, int input) throws IOException {
        int[] arr = new int[input];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(reader.readLine());
        }
        return arr;
    }

    /**
     * Method which print result
     * @param arr
     */
    void printResults(int[] arr) {
        System.out.print("Array Elements in Ascending Order: ");
        for (int i = 0; i < arr.length - 1; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.print(arr[arr.length - 1]);
    }

    /**
     * Method which sort Integer in Ascending order
     * @param arr
     */
     void sortIntegerInAscendingOrder(int[] arr) {
        int temp;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
}
