package com.epam.project;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Class which gets the userInput
 *
 * @author Norbert Borek
 */
public class User {

    /**
     * Method which get input from user
     * @return input
     * @throws IOException
     */
    int getLengthOfArray(int inputUser) {
        if (inputUser > 10 || inputUser < 1) {
            throw new IllegalArgumentException("From 1 to max. ten numbers !");
        }
        return inputUser;
    }
    int getInputUser(BufferedReader reader) throws IOException {
        System.out.print("Enter number of elements you want in the array (Max up to ten numbers): ");
        int inputUser = Integer.parseInt(reader.readLine());
        return inputUser;
    }
}
