package com.epam.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Maven-based project – Sorting App
 * Small Java application that takes up to ten command-line arguments
 * as integer values, sorts them in the ascending order,
 * and then prints them into standard output.
 *
 * Main executable class
 * @author Norbert Borek
 * @version 1.2
 */

public class MainApp {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Calc calc = new Calc();
        User user = new User();

        int inputUser = user.getInputUser(reader);
        int[] arr = calc.getIntsAndAdd(reader, user.getLengthOfArray(inputUser));
        calc.sortIntegerInAscendingOrder(arr);
        calc.printResults(arr);
    }
}
